# RC2014 target for Collapse OS

This is a Collapse OS port for the [RC2014][rc2014]. It features support for:

* The MC6850-based serial communication card
* The Zilog SIO-based serial communication card
* SD card access through handmade SPI relay card

To build, run `make` in this directory. The resulting `rc2014.rom` should be
written to a ROM of at least 8KB.

This repository has a RC2014 emulator, you can launch the built ROM through
that emulator with `make emul`.

By default, the ROM is built and emulated with the MC6850 card. If you want to
use the `SIO=1` option in `make`. Use it for the `emul` target too. Example:

	make clean SIO=1 emul

This port is fully self-hosting, that is, the resulting ROM can build the
resulting ROM.

For the rest, refer to documentation in `doc`.

[rc2014]: https://rc2014.co.uk
