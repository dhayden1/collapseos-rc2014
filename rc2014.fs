( ----- 000 )
RC2014 MASTER INDEX

401 AT28 EEPROM                405 SPI relay
410 MC6850 driver              415 Zilog SIO driver
420 SD Card subsystem
( ----- 001 )
code at28c! ( c a -- )
  BC>HL, BC pop,
  (HL) C ld, A C ld, ( orig ) B C ld, ( save )
  C (HL) ld, ( poll ) begin,
    A (HL) ld, ( poll ) A C cp, ( same as old? )
    C A ld, ( save old poll, Z preserved )
  br jrnz,
\ equal to written? SUB instead of CP to ensure IOERR is NZ
  A B sub, ifnz, SYSVARS ( IOERR ) m) A ld, then, BC pop, ;code
: at28! ( n a -- ) 2dup at28c! 1+ swap >>8 swap at28c! ;
( ----- 005 )
( SPI relay driver. See doc/spi.txt )
code (spix) ( n -- n )
  A C ld,
  SPI_DATA i) A out,
  \ wait until xchg is done
  begin, A SPI_CTL i) in, A 1 i) and, br jrnz,
  A SPI_DATA i) in,
  C A ld, ;code
code (spie) ( n -- ) A C ld, SPI_CTL i) A out, BC pop, ;code
( ----- 010 )
\ MC6850 Driver. Requires:
\ 6850_CTL for control register
\ 6850_IO for data register.
\ CTL numbers used: $16 = no interrupt, 8bit words, 1 stop bit
\ 64x divide. $56 = RTS high
code 6850>
  begin,
    A 6850_CTL i) in, A $02 i) and, ( are we transmitting? )
  br jrz, ( yes, loop )
  A C ld, 6850_IO i) A out, BC pop, ;code
( ----- 011 )
code 6850<? BC push,
  clrA, ( 256x ) A $16 i) ( RTS lo ) ld, 6850_CTL i) A out,
  BC 0 i) ld, ( pre-push a failure )
  begin, AF AF' ex, ( preserve cnt )
    A 6850_CTL i) in, A $1 i) and, ( rcv buff full? )
    ifnz, ( full )
      A 6850_IO i) in, pushA, C 1 i) ld, clrA, ( end loop )
    else, AF AF' ex, ( recall cnt ) A dec, then,
  br jrnz,
  A $56 i) ( RTS hi ) ld, 6850_CTL i) A out, ;code
( ----- 012 )
alias 6850<? rx<? alias 6850<? (key?)
alias 6850> tx> alias 6850> (emit)
: 6850$ $56 ( RTS high ) [ 6850_CTL litn ] pc! ;
( ----- 015 )
( Zilog SIO driver. Requires:
  SIOA_CTL for ch A control register SIOA_DATA for data
  SIOB_CTL for ch B control register SIOB_DATA for data )
code sioa<? BC push,
  clrA, ( 256x ) BC 0 i) ld, ( pre-push a failure )
  A 5 i) ( PTR5 ) ld, SIOA_CTL i) A out,
  A $68 i) ( RTS low ) ld, SIOA_CTL i) A out,
  begin, AF AF' ex, ( preserve cnt )
    A SIOA_CTL i) in, A $1 i) and, ( rcv buff full? )
    ifnz, ( full )
      A SIOA_DATA i) in, pushA, C 1 i) ld, clrA, ( end loop )
    else, AF AF' ex, ( recall cnt ) A dec, then,
  br jrnz,
  A 5 i) ( PTR5 ) ld, SIOA_CTL i) A out,
  A $6a i) ( RTS high ) ld, SIOA_CTL i) A out, ;code
( ----- 016 )
code sioa>
  begin,
    A SIOA_CTL i) in, A $04 i) and, ( are we transmitting? )
  br jrz, ( yes, loop )
  A C ld, SIOA_DATA i) A out, BC pop, ;code
create _ ( init data ) $18 c, ( CMD3 )
    $24 c, ( CMD2/PTR4 ) $c4 c, ( WR4/64x/1stop/nopar )
    $03 c, ( PTR3 ) $c1 c, ( WR3/RXen/8char )
    $05 c, ( PTR5 ) $6a c, ( WR5/TXen/8char/RTS )
    $21 c, ( CMD2/PTR1 ) 0 c, ( WR1/Rx no INT )
: sioa$ _ >A 9 for Ac@+ [ SIOA_CTL litn ] pc! next ;
( ----- 017 )
CODE siob<? BC push, ( copy/paste of SIOA<? )
  clrA, ( 256x ) BC 0 i) ld, ( pre-push a failure )
  A 5 i) ( PTR5 ) ld, SIOB_CTL i) A out,
  A $68 i) ( RTS low ) ld, SIOB_CTL i) A out,
  begin, AF AF' ex, ( preserve cnt )
    A SIOB_CTL i) in, A $1 i) and, ( rcv buff full? )
    ifnz, ( full )
      A SIOB_DATA i) in, pushA, C 1 i) ld, clrA, ( end loop )
    else, AF AF' ex, ( recall cnt ) A dec, then,
  br jrnz,
  A 5 i) ( PTR5 ) ld, SIOB_CTL i) A out,
  A $6a i) ( RTS high ) ld, SIOB_CTL i) A out, ;code
( ----- 018 )
code siob>
  begin,
    A SIOB_CTL i) in, A $04 i) and, ( are we transmitting? )
  br jrz, ( yes, loop )
  A C ld, SIOB_DATA i) A out, BC pop, ;code
: siob$ _ >A 9 for Ac@+ [ SIOB_CTL litn ] pc! next ;
( ----- 020 )
\ SD Card subsystem
SDC_MEM constant SDC_SDHC
: _idle ( -- n ) $ff (spix) ;

( spix $ff until the response is something else than $ff
  for a maximum of 20 times. Returns $ff if no response. )
: _wait ( -- n )
  0 ( dummy ) 20 for
    drop _idle dup $ff = not if leave then next ;

( adjust block for LBA for SD/SDHC )
: _badj ( arg1 arg2 -- arg1 arg2 )
  SDC_SDHC @ if 0 swap else dup 128 / swap <<8 << then ;
( ----- 021 )
( The opposite of sdcWaitResp: we wait until response is $ff.
  After a successful read or write operation, the card will be
  busy for a while. We need to give it time before interacting
  with it again. Technically, we could continue processing on
  our side while the card it busy, and maybe we will one day,
  but at the moment, I'm having random write errors if I don't
  do this right after a write, so I prefer to stay cautious
  for now. )
: _ready ( -- ) begin _idle $ff = until ;
( ----- 022 )
( Computes n into crc c with polynomial $09
  Note that the result is "left aligned", that is, that 8th
  bit to the "right" is insignificant (will be stop bit). )
: _crc7 ( c n -- c )
  xor 8 for ( c )
    << ( c<<1 ) dup >>8 if
      ( MSB was set, apply polynomial )
      <<8 >>8 $12 xor ( $09 << 1, we apply CRC on high bits )
    then next ;
( send-and-crc7 )
: _s+crc ( n c -- c ) swap dup (spix) drop _crc7 ;
( ----- 023 )
( cmd arg1 arg2 -- resp )
( Sends a command to the SD card, along with arguments and
  specified CRC fields. (CRC is only needed in initial commands
  though). This does *not* handle CS. You have to
  select/deselect the card outside this routine. )
: _cmd
    _wait drop rot    ( a1 a2 cmd )
    0 _s+crc          ( a1 a2 crc )
    rot l|m rot       ( a2 h l crc )
    _s+crc _s+crc     ( a2 crc )
    swap l|m rot      ( h l crc )
    _s+crc _s+crc     ( crc )
    1 or              ( ensure stop bit )
    (spix) drop       ( send CRC )
    _wait  ( wait for a valid response... ) ;
( ----- 024 )
( cmd arg1 arg2 -- r )
( Send a command that expects a R1 response, handling CS. )
: sdcmdr1 [ SDC_DEVID litn ] (spie) _cmd 0 (spie) ;

( cmd arg1 arg2 -- r arg1 arg2 )
( Send a command that expects a R7 response, handling CS. A R7
  is a R1 followed by 4 bytes. arg1 contains bytes 0:1, arg2
  has 2:3 )
: sdcmdr7
    [ SDC_DEVID litn ] (spie)
    _cmd                 ( r )
    _idle <<8 _idle +  ( r arg1 )
    _idle <<8 _idle +  ( r arg1 arg2 )
    0 (spie) ;
: _rdsdhc ( -- ) $7a ( CMD58 ) 0 0 sdcmdr7 drop $4000
  and SDC_SDHC ! drop ;
( ----- 025 )
: _err 0 (spie) S" SDerr" stype abort ;

( Tight definition ahead, pre-comment.

  Initialize a SD card. This should be called at least 1ms
  after the powering up of the card. We begin by waking up the
  SD card. After power up, a SD card has to receive at least
  74 dummy clocks with CS and DI high. We send 80.
  Then send cmd0 for a maximum of 10 times, success is when
  we get $01. Then comes the CMD8. We send it with a $01aa
  argument and expect a $01aa argument back, along with a
  $01 R1 response. After that, we need to repeatedly run
  CMD55+CMD41 ($40000000) until the card goes out of idle
  mode, that is, when it stops sending us $01 response and
  send us $00 instead. Any other response means that
  initialization failed. )
( ----- 026 )
: sdc$
  10 for _idle drop next
  0 ( dummy ) 10 for ( r )
    drop $40 0 0 sdcmdr1  ( CMD0 )
    1 = dup if leave then
  next not if _err then
  $48 0 $1aa ( CMD8 ) sdcmdr7 ( r arg1 arg2 )
  ( expected 1 0 $1aa )
  $1aa = rot ( arg1 f r ) 1 = and swap ( f&f arg1 )
  not ( 0 expected ) and ( f&f&f ) not if _err then
  begin
    $77 0 0 sdcmdr1  ( CMD55 )
    1 = not if _err then
    $69 $4000 0 sdcmdr1  ( CMD41 )
    dup 1 > if _err then
  not until _rdsdhc ; ( out of idle mode, success! )
( ----- 027 )
:~ ( dstaddr blkno -- )
  [ SDC_DEVID litn ] (spie)
  $51 ( CMD17 ) swap _badj ( a cmd arg1 arg2 ) _cmd if _err then
  _wait $fe = not if _err then
  >A 0 512 for ( crc1 )
    _idle ( crc1 b ) dup Ac!+ ( crc1 b ) crc16 next ( crc1 )
    _idle <<8 _idle + ( crc1 crc2 )
    _wait drop 0 (spie) = not if _err then ;
: sdc@ ( blkno blk( -- )
  swap << ( 2x ) 2dup ( a b a b ) ~
  ( a b ) 1+ swap 512 + swap ~ ;
( ----- 028 )
:~ ( srcaddr blkno -- )
  [ SDC_DEVID litn ] (spie)
  $58 ( CMD24 ) swap _badj ( a cmd arg1 arg2 ) _cmd if _err then
  _idle drop $fe (spix) drop
  >A 0 512 for ( crc )
    Ac@+ ( crc b ) dup (spix) drop crc16 next ( crc )
    dup >>8 ( crc msb ) (spix) drop (spix) drop
    _wait $1f and 5 = not if _err then _ready 0 (spie) ;
: sdc! ( blkno blk( -- )
  swap << ( 2x ) 2dup ( a b a b ) ~
  ( a b ) 1+ swap 512 + swap ~ ;
