\ RC2014 classic with MC6850
8 consts $ff00 RS_ADDR $fffa PS_ADDR $8000 HERESTART
         $80 6850_CTL $81 6850_IO
         4 SPI_DATA 5 SPI_CTL 1 SDC_DEVID
RS_ADDR $90 - value SYSVARS
SYSVARS $409 - value BLK_MEM
SYSVARS $80 + value SDC_MEM
xcomp z80a
xcompc z80c corel
410 412 loadr \ MC6850
405 load \ SPI relay
420 428 loadr \ SD Card
401 load \ AT28
alias sdc@ (blk@)
alias sdc! (blk!)
blksub
: init 6850$ blk$ ;
