\ RC2014 classic with SIO
10 consts $ff00 RS_ADDR $fffa PS_ADDR $8000 HERESTART
         $80 SIOA_CTL $81 SIOA_DATA
         $82 SIOB_CTL $83 SIOB_DATA
         4 SPI_DATA 5 SPI_CTL 1 SDC_DEVID
RS_ADDR $90 - value SYSVARS
SYSVARS $409 - value BLK_MEM
SYSVARS $80 + value SDC_MEM
xcomp z80a
xcompc z80c corel
415 418 loadr \ SIO
405 load \ SPI relay
420 428 loadr \ SD Card
401 load \ AT28
alias sdc@ (blk@)
alias sdc! (blk!)
blksub
: init sioa$ blk$ ;
alias sioa<? (key?) alias sioa> (emit)
alias sioa<? rx<? alias sioa> tx>
