TARGETS = rc2014.rom
CONF ?= mc6850
ifdef SIO
CONF = sio
EMUL_ARGS = -s
endif

all: $(TARGETS)

duskos/README.md:
	git submodule init
	git submodule update

duskos/cos/extra.fs: rc2014.fs
	cp rc2014.fs $@

fs: duskos/README.md
	ln -sf duskos/fs .

posix: duskos/README.md
	ln -sf duskos/posix .

duskos/dusk: fs posix duskos/cos/extra.fs
	$(MAKE) -C duskos dusk

rc2014.rom: duskos/dusk buildpre.fs buildpost.fs conf/$(CONF).fs
	dd if=/dev/zero of=$@ bs=1K count=8
	cat buildpre.fs conf/$(CONF).fs buildpost.fs | ./duskos/dusk

.PHONY: emul
emul: rc2014.rom
	$(MAKE) -C emul rc2014
	./emul/rc2014 $(EMUL_ARGS) -cfs/cos/blk rc2014.rom

.PHONY: clean
clean:
	$(MAKE) -C duskos clean
	$(MAKE) -C emul clean
	rm -f $(TARGETS)
